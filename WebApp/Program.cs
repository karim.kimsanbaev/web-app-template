using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

#region | Services & Controllers |

builder.Services.AddControllers();

#endregion

#region | AddSwaggerGen |

builder.Services.AddSwaggerGen(options =>
{
    // https://learn.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-7.0&tabs=visual-studio
    var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
});

#endregion

var app = builder.Build();

#region | IsDevelopment |

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();

    app.UseDeveloperExceptionPage();

    app.MapFallback((context) =>
        {
            context.Response.Redirect("/swagger");
            return Task.CompletedTask;
        }
    );
}

#endregion

app.MapControllers();

app.Run();